extern crate rand;

use std::collections::HashMap;
use rand::prelude::*;
use std::io;
use std::io::prelude::*;


fn main() {

    // exercise2();
    let mut stuff = String::new();
    let stdin = io::stdin;
    println!("Type something: ");
    stdin().lock().read_line(&mut stuff).unwrap();
    println!("{}", stuff);

}

#[allow(dead_code)]
fn inteweb() {
    let mut chars = "Sometimes dignity is the price we pay for a good night out!".chars().peekable();
    let mut new_s = String::new();
    while let Some(c) = chars.next() {
        let suffix = match c {
            'a' | 'e' | 'i' | 'o' | 'u' => {
                new_s.push(c);
                String::from("-hay")
            }
            'a'...'z' | 'A'...'Z' => {
                format!("-{}ay", c)
            }
            _ => {
                new_s.push(c);
                continue;
            }
        };

        while let Some(&c) = chars.peek() {
            match c {
                'a'...'z' | 'A'...'Z' => {
                    chars.next();
                    new_s.push(c);
                }
                _ => break,
            }
        }
        new_s += &suffix;
    }
    println!("{}", new_s);
}


fn is_vowel(c: char) -> bool {
    //println!("Char being checked: {}", c);
    match "aeiouAEIOU".find(c) {
        None => {false},
        _ => true,
    }
}

#[allow(dead_code)]
fn exercise2() {
    println!("\nExercise #2...");
    let sentence = "Sometimes dignity is the price we pay for a good night out";
    let mut final_string = String::new();

    for word in sentence.split_whitespace() {
        let mut new_word = String::new();

        let first_letter: char = match word.chars().nth(0) {
            Some(letter) => letter.clone(),
            _ => '0',
        };

        if is_vowel(first_letter) {
            // Just add "hay"
            new_word.push_str(&word);
            new_word.push_str("hay");

        } else {
            // move first letter to end and add "hay"
            let b = [first_letter as u8];
            let s = std::str::from_utf8(&b).unwrap();
            new_word.push_str(&word[1..]);
            new_word.push_str(s);
            new_word.push_str("ay");
        }
        final_string.push_str(&new_word.to_lowercase());
        final_string.push_str(" ");
    }
    println!("Original: {}\nPiglantin: {}",sentence,final_string);
}

#[allow(dead_code)]
fn exercise1(){
    println!("Exercise #1...");
    let mut nums = generate_numbers();
    // println!("{:?}", nums);
    nums.sort();
    let mean = get_mean(&nums);
    let median = get_median(&nums);
    let mode = get_mode(&nums);
    println!("Mean: {}; Median: {}; Mode: {:?}", mean, median, mode);
}

fn generate_numbers() -> Vec<i32> {
    let mut v: Vec<i32> = Vec::new();
    let mut rng = rand::thread_rng();
    for _index in 0..50 {
        v.push(rng.gen_range(0,100));
    }
    return v;
}

fn get_mean(num_vector: &Vec<i32>) -> f32 {
    let sum: i32 = num_vector.iter().sum();
    sum as f32 / num_vector.len() as f32
}

fn get_median (num_vector: &Vec<i32>) -> i32 {
    num_vector[num_vector.len() / 2]
}

fn get_mode (num_vector: &Vec<i32>) -> Vec<i32> {
    let mut map = HashMap::new();
    let mut modes: Vec<i32> = Vec::new();
    let mut last_max = -1;
    for number in num_vector {
        let count = map.entry(*number).or_insert(0);
        *count += 1;
        if count >= &mut last_max {
            if count > &mut last_max {modes.clear()}
            last_max = *count;
            modes.push(*number);
        }
    }
    modes
}

